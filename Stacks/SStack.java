/**
 * SStack.java
 * Part of the stacks family
 *
 * SStack Java class, which contains methods to create and use the stacks
 * All of these methods are declared in the IStack Java interface
 * http://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */

package stack;

public class SStack implements IStack {

	SNode peak;
	
	public void push(String elem) {
		SNode newNode = new SNode(elem);
		newNode.next = peak;
		peak = newNode;
		System.out.println("Element added successfully");
		
	}

	public String top() {
		if (isEmpty()){
			System.out.println("WARNING: Stack is empty");
			return null;
		}
		else return peak.elem;
	}

	public String pop() {
		if (isEmpty()){
			System.out.println("WARNING: Stack is empty");
			return null;
		}
		
		String var = top();
		peak = peak.next;
		System.out.println("Element removed successfully");
		return var;
		
	}

	public int getSize() {
		int size = 0;
		for (SNode point = peak; point != null; point = point.next){
			size++;
		}
		return size;
	}

	public boolean isEmpty() {
		return peak == null;
	}

	public String toString(){
		String r = "";
		for (SNode point = peak; point != null; point = point.next){
			r += point.elem + " ";
		}
		return r;
	}
}
