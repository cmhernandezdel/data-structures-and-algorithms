/**
 * IStack.java
 * Part of the stack family
 *
 * Stack Java interface, which contains all the methods to create and use stacks
 * http://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */

package stack;

public interface IStack {

	public void push (String elem);
	
	public String top();
	
	public String pop();
	
	public int getSize();
	
	public boolean isEmpty();
	
}
