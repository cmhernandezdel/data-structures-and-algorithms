<h2> Data structures and algorithms </h2>
Basic data structures and some algorithms from the DSA course at Universidad Carlos III de Madrid.

Feel free to view, use and redistribute this code in any way.

Solved in 2015 using Java. 
<hr>
<h3> Data structures </h3>
Note: We use String items as example, but it could be whatever you want, or you could use generic programming and let the compiler do the job.
<p></p>
<h5> Single linked list </h5>
The most simple data structure. It consists in a list in which you can insert, delete or access in any index. Single linked means that you can access the next node from the current one, but not the previous one.

<h5> Double linked list </h5>
Similar to the single linked list, but we can also access the previous node from the current one.
For more information on lists, see <a href="https://en.wikipedia.org/wiki/List_(abstract_data_type)">the wikipedia article</a>.

<h5> Queues </h5>
Like a real life queue, items can only be inserted at the tail of the queue and retrieved from the head. This is called <a href="https://en.wikipedia.org/wiki/FIFO_(computing_and_electronics)">FIFO</a> data structure.
For more information on queues, see <a href="https://en.wikipedia.org/wiki/Queue_(abstract_data_type)"> the wikipedia article</a>.

<h5> Stacks </h5>
In this data structure, items can only be inserted at the top of the stack (called the head) and retrieved from the top as well. Whenever we get an item from the stack, it gets removed.
For more information on stacks, see <a href="https://en.wikipedia.org/wiki/Stack_(abstract_data_type)"> the wikipedia article</a>.

<h5> Binary search trees </h5>
They allow for quick access to items, because they're sorted. For this reason, the complexity isn't linear as in other data structures, but rather logarithmic. However, it is important to keep them ordered, and this can take time. Therefore, they are good for searching for items, but bad at inserting new items.
For more information on trees, see <a href="https://en.wikipedia.org/wiki/Binary_search_tree"> the wikipedia article</a>.

<hr>

<h3> Algorithms </h3>
<h5> Quicksort </h5>
This algorithm is used to sort arrays. It's based in comparisons, so you can use it with numbers, but not with Strings, for example, unless you define the relationship between them (i.e., having less vowels).
Has O(n*log2 n) complexity. You can see the explanation of the algorithm in <a href="https://en.wikipedia.org/wiki/Quicksort"> Wikipedia</a>.

<h5> Mergesort </h5>
It is also an algorithm used for array sorting, generally with the same complexity as Quicksort as seen in <a href="https://en.wikipedia.org/wiki/Merge_sort">Wikipedia</a>.
Here, it is divided into the merge and sort methods, but it could be implemented in only one method.
It is also comparison-based.