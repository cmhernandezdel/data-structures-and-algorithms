/**
 * DNode.java
 * Part of the double linked list family
 *
 * DNode Java class, which contains an element of type String
 * It also contains pointers to previous and next nodes
 * http://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */

package lists.doubleLinkedList;

public class DNode {

	String elem;
	DNode prev, next;
	
	public DNode(String elem){
		this.elem = elem;
	}
	
}
