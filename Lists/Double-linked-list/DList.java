/**
 * DList.java
 * Part of the double linked list family
 *
 * DList Java class, which contains methods to create and use the double linked lists
 * All of these methods are declared in the IList Java interface
 * http://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */

package lists.doubleLinkedList;

public class DList implements IList {
	
	DNode header, trailer;
	public DList(){
		header = new DNode(null);
		trailer = new DNode(null);
		header.next = trailer;
		trailer.prev = header;
	}

	public void addFirst(String newElem) {
	    DNode newNode = new DNode(newElem);
	    newNode.next = header.next;
	    newNode.prev = header;
	    header.next.prev = newNode;
	    header.next = newNode;
	    System.out.println("Element added successfully");
		
	}

	public void addLast(String newElem) {
		DNode newNode = new DNode(newElem);
		newNode.next = trailer;
		newNode.prev = trailer.prev;
		trailer.prev.next = newNode;
		trailer.prev = newNode;
		System.out.println("Element added successfully");
		
	}

	public void insertAt(int index, String newElem) {
		if (index < 0 || index > getSize()){
			System.out.println("WARNING: Wrong index");
			return;
		}
		
		int i = 0;
		DNode newNode = new DNode(newElem);
		for (DNode point = header.next; point != trailer; point = point.next){
			if (i == index){
				newNode.prev = point.prev;
				newNode.next = point;
				point.prev.next = newNode;
				point.prev = newNode;
				System.out.println("Element added successfully");
			}
			i++;
		}
		
		
	}

	public boolean isEmpty() {
		return header.next == trailer;
	}

	public boolean contains(String elem) {
		return !(getIndexOf(elem) == -1);
	}

	public int getSize() {
		DNode point = header.next;
		int size = 0;
		while (point != trailer){
			size++;
			point = point.next;
		}
		return size;
	}

	public int getIndexOf(String elem) {
		int index = 0;
		for (DNode point = header.next; point != trailer; point = point.next){
			if (point.elem.equals(elem)) return index;
			index++;
		}
		return -1;
	}

	public String getFirst() {
		if (isEmpty()){
			System.out.println("WARNING: List is empty");
			return null;
		}
		return header.next.elem;
	}

	public String getLast() {
		if (isEmpty()){
			System.out.println("WARNING: List is empty");
			return null;
		}
		return trailer.prev.elem;
	}

	public String getAt(int index) {
		int i = 0;
		for (DNode point = header.next; point != trailer; point = point.next){
			if (i == index) return point.elem;
			i++;
		}
		System.out.println("WARNING: Wrong index");
		return null;
	}

	public void removeFirst() {
		if (isEmpty()){
			System.out.println("WARNING: List is empty");
			return;
		}
		
		header.next.next.prev = header;
		header.next = header.next.next;
		System.out.println("Element removed successfully");
		
	}

	public void removeLast() {
		if (isEmpty()){
			System.out.println("WARNING: List is empty");
			return;
		}
		
		trailer.prev.prev.next = trailer;
		trailer.prev = trailer.prev.prev;
		System.out.println("Element removed successfully");
		
	}

	public void removeAll(String elem) {
		for (DNode point = header; point.next != trailer; point = point.next){
			if (point.next.elem == elem){
				point.next.next.prev = point;
				point.next = point.next.next;
				System.out.println("Element removed successfully");
			}
		}
		
	}

	public void removeAt(int index) {
		int i = -1;
		for (DNode point = header; point.next != trailer; point = point.next){
			if (i == index-1){
				point.next.next.prev = point;
				point.next = point.next.next;
				System.out.println("Element removed successfully");
			}
		}
		
	}
	
	public String toString(){
		String r="";
		for (DNode point = header.next; point != trailer; point = point.next){
			r = r + point.elem + " ";
		}
		return r;
	}

	
}
