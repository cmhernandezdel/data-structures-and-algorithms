/**
 * IList.java
 * Part of the single and double linked list family
 *
 * List Java interface, which contains all the methods to create and use lists
 * http://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */


package lists;

public interface IList {

	public void addFirst(String newElem);

	public void addLast(String newElem);

	public void insertAt(int index, String newElem);
	
	
	
	public boolean isEmpty();

	public boolean contains(String elem);

	public int getSize();

	public int getIndexOf(String elem);

	public String getFirst();

	public String getLast();

	public String getAt(int index);
	
	public String toString();

	
	public void removeFirst();

	public void removeLast();

	public void removeAll(String elem);

	public void removeAt(int index);

}
