/**
 * SList.java
 * Part of the single linked list family
 *
 * SList Java class, which contains methods to create and use the single linked lists
 * All of these methods are declared in the IList Java interface
 * http://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */

package lists.singleLinkedList;

public class SList implements IList {

	SNode first;
	
	public void addFirst(String elem){
		SNode newNode = new SNode(elem);
		newNode.next = first;
		first = newNode;
		System.out.println("Element added successfully");
	}
	
	public void addLast(String elem){
		if (isEmpty()) addFirst(elem);
		else{
			SNode aux = first, newNode = new SNode(elem);
			while (aux.next != null) aux = aux.next;
			aux.next = newNode;
			System.out.println("Element added successfully");
		}
	}
	
	public void insertAt(int index, String elem){
		if (index < 0 || index > getSize()){
			System.out.println("WARNING: Wrong index");
			return;
		}
		if (index == 0){
			addFirst(elem);
			return;
		}
		
		SNode newNode = new SNode(elem);
		SNode aux = first;
		int i = 0;
		while (i < index-1){
			aux = aux.next;
			i++;
		}
		newNode.next = aux.next;
		aux.next = newNode;
		System.out.println("Element added successfully");
		
	}
	
	public boolean isEmpty(){
		return first == null;
	}
	
	public boolean contains(String elem){
		if (getIndexOf(elem) == -1) return false;
		return true;
	}
	
	public int getSize(){
		int size = 0;
		for (SNode point = first; point != null; point = point.next){
			size++;
		}
		return size;
	}
	
	public int getIndexOf(String elem){
		int index = 0;
		SNode point = first;
		while (point != null){
			if (point.elem.equals(elem)) return index;
			index++;
		}
		return -1;
	}
	
	public String getFirst(){
		if (isEmpty()){
			System.out.println("WARNING: List is empty");
			return null;
		}
		return first.elem;
	}
	
	public String getLast(){
		if (isEmpty()){
			System.out.println("WARNING: List is empty");
			return null;
		}
		SNode point = first;
		while (point.next != null) point = point.next;
		return point.elem;
	}
	
	public String getAt (int index){
		if (index < 0 || index >= getSize())
			System.out.println("WARNING: Wrong index");	
		
		SNode point = first;
		for (int i = 0; i < getSize(); i++){
			if (i == index) return point.elem;
		}
		return null;
	}
	
	public String toString(){
		String r ="";
		for (SNode point = first; point != null; point = point.next){
			r += point.elem + " ";
		}
		
		return r;
	}
	
	public void removeFirst(){
		if (isEmpty()){
			System.out.println("WARNING: List is empty");
			return;
		}
		
		first = first.next;
		System.out.println("Element removed successfully");
	}
	
	public void removeLast(){
		if (isEmpty()){
			System.out.println("WARNING: List is empty");
			return;
		}
		SNode point = first, aux = first;
		while (point.next != null){
			aux = point;
			point = point.next;
		}
		aux.next = null;
		point = null;
		System.out.println("Element removed successfully");
	}
	
	public void removeAll(String elem){
		if (isEmpty()){
			System.out.println("WARNING: List is empty");
			return;
		}
		
		if (!contains(elem)){
			System.out.println("WARNING: List does not contain this element");
			return;
		}
		
		for (SNode point = first; point.next != null; point = point.next){
			if (point.next.elem.equals(elem)) {
				point.next = point.next.next;
				System.out.println("Element removed successfully");
			}
			
		}
		
		if (first.elem.equals(elem)){
			removeFirst();
		}
		
	}
	
	public void removeAt(int index){
		if (index < 0 || index >= getSize()){
			System.out.println("WARNING: Wrong index");
			return;
		}
		
		if (index == 0) removeFirst();
		
		else{
			int i = 0;
			for (SNode point = first; point.next != null; point = point.next){
				if (i+1 == index){
					point.next = point.next.next;
				}
				i++;
			}
		}
	}
}
