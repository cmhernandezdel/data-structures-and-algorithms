/**
 * IBSTree.java
 * Part of the binary search tree family
 *
 * BSTree Java interface, which contains all the methods to create and use binary search trees
 * http://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */

package binarySearchTrees;

public interface IBSTree {

	public int getHeight();
	
	public int getSize(BSTNode subtree);
	
	public int getDepth (BSTNode node);
	
	public void preOrder();
	
	public void inOrder();
	
	public void postOrder();
	
	public void levelOrder();
	
	public void levelOrderRec();
	
	public String find(int key);
	
	public String findRec(int key);
	
	public void remove(int key);
	
	public void insert(String elem, int key);
}
