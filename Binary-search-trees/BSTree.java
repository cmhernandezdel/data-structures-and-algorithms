/**
 * BSTree.java
 * Part of the binary search tree family
 *
 * BSTree Java class, which contains methods to create and use the binary search trees
 * All of these methods are declared in the IBSTree Java interface
 * This class needs to import SList and queues from LinkedList
 * http://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */

package binarySearchTrees;
import lists.singleLinkedList.SList;
import java.util.LinkedList;

public class BSTree implements IBSTree{

	BSTNode root;

	/**
	 * Method that allows us to know the height of a tree
	 * The height of a tree is equal to the height of its root
	 * Calls a private method that returns the height counting from a specific node
	 * @return the height of the tree
	 */
	public int getHeight() {
		return getHeight(root);
	}
	
	private int getHeight(BSTNode node){
		if (node == null) return 0;
		else return (1 + Math.max(getHeight(node.left), getHeight(node.right)));
	}

	/**
	 * Method that allows us to know the size of a given subtree
	 * The size of a subtree is the number of nodes below its root
	 * @param subtree The subtree whose size we want to know
	 * @return the size
	 */
	public int getSize(BSTNode subtree) {
		if (subtree == null) return 0;
		else return (1 + getSize(subtree.right) + getSize(subtree.left));
	}

	/**
	 * Method that allows us to know the depth of a given node
	 * The depth of a node is basically the level in which it is located
	 * We'll be considering that the level of the root is zero
	 * @param node The node whose depth we want to know
	 * @return the depth
	 */
	public int getDepth(BSTNode node) {
		if (node == null) return -1;
		else return (1 + getDepth(node.parent));
	}

	/**
	 * Method that goes through the tree in a pre-order manner
	 * This means, first we visit the node, then we visit its children
	 * Calls a private method and uses a list to store the elements if we want them for something
	 */
	public void preOrder() {
		SList list = new SList();
		preOrder(root, list);
		
	}
	
	private void preOrder(BSTNode node, SList list){
		if (node == null) return;
		
		System.out.println(node.elem);
		list.addLast(node.elem);
		preOrder(node.left, list);
		preOrder(node.right, list);
	}

	/**
	 * Method that goes through the tree in an in-order manner
	 * This means, first we visit the left child, then we visit the node, then the right child
	 * Calls a private method and uses a list to store the elements if we want them for something
	 */
	public void inOrder() {
		SList list = new SList();
		inOrder(root, list);
		
	}
	
	private void inOrder(BSTNode node, SList list){
		if (node == null) return;
		
		inOrder(node.left, list);
		System.out.println(node.elem);
		list.addLast(node.elem);
		inOrder(node.right, list);
	}

	/**
	 * Method that goes through the tree in a post-order manner
	 * This means, first we visit the children, then we visit the node
	 * Calls a private method and uses a list to store the elements if we want them for something
	 */
	public void postOrder() {
		SList list = new SList();
		postOrder(root, list);
		
	}
	
	private void postOrder(BSTNode node, SList list){
		if (node == null) return;
		
		postOrder(node.left, list);
		postOrder(node.right, list);
		System.out.println(node.elem);
		list.addLast(node.elem);
	}

	/**
	 * Method that goes through the tree in a level manner
	 * This means, we show the root, then its children, then the children of its children...
	 * Iterative way to implement it
	 */
	public void levelOrder() {
		LinkedList queue = new LinkedList();
		queue.add(root);
		
		while (!queue.isEmpty()){
			BSTNode front = (BSTNode) queue.poll();
			System.out.println(front.elem);
			if (front.left != null) queue.add(front.left);
			if (front.right != null) queue.add (front.right);
		}
		
	}

	/**
	 * Method that goes through the tree in a level manner
	 * This means, we show the root, then its children, then the children of its children...
	 * Recursive way to implement it
	 * Calls a private method
	 */
	public void levelOrderRec() {
		levelOrderRec(root);
		
	}
	
	private void levelOrderRec(BSTNode node){
		if (node == null) return;
		for (int i = 0; i < getHeight(); i++){
			levelOrderRec(node, i);
		}
	}
	
	private void levelOrderRec(BSTNode node, int level){
		if (node == null) return;
		if (level == 0) System.out.println(node.elem);
		else{
			levelOrderRec(node.left, level-1);
			levelOrderRec(node.right, level-1);
		}
	}

	/**
	 * Method that finds the element associated to a given key
	 * If it cannot find it, it shows an error message
	 * @param key, the key whose element we want to retrieve
	 * @return the element
	 */
	public String find(int key) {
		BSTNode point = root;
		
		while (point != null){
			if (point.key == key){
				System.out.println("Found element!");
				System.out.println("Element " + point.elem + " with key " + point.key);
				return point.elem;
			}
			else if (key < point.key) point = point.left;
			else point = point.right;
		}
		System.out.println("Could not find element, try with another key");
		System.out.println("Exiting...");
		return null;
	}
	
	/**
	 * Method that finds the element associated to a given key
	 * If it cannot find it, it shows an error message
	 * Calls to a private method
	 * @param key, the key whose element we want to retrieve
	 * @return the element
	 */
	public String findRec(int key){
		return findRec(key, root);
	}
	
	private String findRec(int key, BSTNode node){
		if (node == null){
			System.out.println("Could not find element, try with another key");
			System.out.println("Exiting...");
			return null;
		}
		if (node.key == key){
			System.out.println("Found element!");
			System.out.println("Element " + node.elem + " with key " + node.key);
			return node.elem;
		}
		
		else if (key < node.key) return findRec(key, node.left);
		else return findRec(key, node.right);
	}

	/**
	 * Method that removes a given key of the tree
	 * Calls to a private method
	 * We will use the successor when the node we want to remove has two children
	 * There is another implementation with the predecessor, but we won't be using it here
	 * @param key, the key we want to remove
	 */
	public void remove(int key) {
		if (root == null){
			System.out.println("Cannot remove because the tree is empty");
			return;
		}
		else if (root.key == key) {
			if (root.left == null && root.right == null){
				root = null;
				System.out.println("Removed successfully");
			}
			else if (root.left == null && root.right != null){
				root = root.right;
				root.parent = null;
				System.out.println("Removed successfully");
			}
			else if (root.left != null && root.right == null){
				root = root.left;
				root.parent = null;
				System.out.println("Removed successfully");
			}
			
			else remove(key, root);
		}
		else remove(key, root);
		
	}
	
	private boolean remove(int key, BSTNode node){
		if (node == null){
			System.out.println("Could not find key. Try again please.");
			return false;
		}
		
		if (key < node.key) return remove(key, node.left);
		if (key > node.key) return remove(key, node.right);
		
		if (node.left == null && node.right == null){
			BSTNode parent = node.parent;
			if (parent.left == node) parent.left = null;
			else parent.right = null;
			System.out.println("Removed successfully");
			return true;
		}
		
		else if (node.left != null && node.right == null){
			BSTNode grandParent = node.parent, grandChild = node.left;
			if (grandParent.left == node) grandParent.left = grandChild;
			else grandParent.right = grandChild;
			grandChild.parent = grandParent;
			System.out.println("Removed successfully");
			return true;
		}
		
		else if (node.left == null && node.right != null){
			BSTNode grandParent = node.parent, grandChild = node.right;
			if (grandParent.left == node) grandParent.left = grandChild;
			else grandParent.right = grandChild;
			grandChild.parent = grandParent;
			System.out.println("Removed successfully");
			return true;
		}
		
		else{
			BSTNode successor = findMin(node.right);
			node.elem = successor.elem;
			node.key = successor.key;
			System.out.println("Removed successfully");
			remove(successor.key, node.right);
			return true;
		}
		
		
	}
	
	private BSTNode findMin(BSTNode node){
		BSTNode point = node;
		while (point.left != null){
			point = point.left;
		}
		return point;
	}

	/**
	 * Method that inserts a given element with a given key in the tree
	 * Calls to a private method
	 * Important annotation: Trees CANNOT store the same key twice
	 * @param elem the element we want to store
	 * @param key, the key we want to assign to the element
	 */
	public void insert(String elem, int key) {
		BSTNode node = new BSTNode(elem, key);
		if (root == null) root = node;
		else insert(node, root);
		
	}
	
	private void insert(BSTNode node, BSTNode start){
		if (start.key == node.key){
			System.out.println("Duplicates are NOT allowed");
			System.out.println("Returning...");
			return;
		}
		
		else if (node.key < start.key){
			if (start.left == null){
				start.left = node;
				node.parent = start;
				System.out.println("Element added successfully!");
			}
			else insert(node, start.left);
			
		}
		
		else{
			if (start.right == null){
				start.right = node;
				node.parent = start;
				System.out.println("Element added successfully!");
			}
			else insert(node, start.right);
		}
	}
}
