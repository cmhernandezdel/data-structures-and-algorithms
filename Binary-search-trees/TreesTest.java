/**
 * TreesTest.java
 * Part of the binary search tree family
 *
 * Binary search trees test class, which contains calls to those methods defined in the other classes
 * http://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */

package binarySearchTrees;

public class TreesTest {

	public static void main (String[] args){
		BSTree tree = new BSTree();
		tree.insert("A", 57);
		tree.insert("B", 24);
		tree.insert("C", 10);
		tree.insert("D", 17);
		tree.insert("E", 66);
		tree.insert("F", 65);
		tree.insert("G", 91);
		tree.insert("G", 57);
		tree.insert("H", 61);
		tree.insert("I", 1);
		tree.insert("V", 5);
		tree.insert("LL", 100);
		tree.insert("L", 50);
		
		tree.inOrder();
		
		System.out.println(tree.getHeight());
		System.out.println(tree.getDepth(tree.root.left.left));
		System.out.println(tree.getSize(tree.root));
		
		tree.find(100);
		tree.remove(100);
		tree.remove(192);
		tree.insert("Z", 100);
		tree.find(100);
		
		tree.remove(66);
		System.out.println(tree.root.right.elem);
		tree.remove(57);
		System.out.println(tree.root.elem);
		
		System.out.println("Test completed successfully");
		
	}
}
