/**
 * BSTNode.java
 * Part of the binary search tree family
 *
 * BSTNode Java class, which contains an element and a key
 * It also contains pointers to the parent and the left and right children
 * http://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */

package binarySearchTrees;
public class BSTNode {

	String elem;
	int key;
	BSTNode parent, left, right;
	
	public BSTNode(String elem, int key){
		this.elem = elem;
		this.key = key;
	}
}
