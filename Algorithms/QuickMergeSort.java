/**
 * QuickMergeSort.java
 * Part of the algorithms family
 *
 * QuickMergeSort Java class, which contains methods for Quicksort algorithm and Mergesort algorithm
 * Search online for more information on these methods for sorting arrays
 * Uncomment lines 128 or 129 to test each one
 * http://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */

package algorithms;

public class QuickMergeSort {

	/**
	 * Quicksort method
	 * @param array, the array we want to sort
	 * @param start, position where we start sorting
	 * @param end, position where we end sorting
	 * @return the sorted array
	 */
	public static int[] quicksort(int[] array, int start, int end){
		int i = start, j = end;
		int pivot = array[start];
		while (i < j){
			while(array[i] <= pivot && i < j) i++;
			while(array[j] > pivot) j--;
			if (i < j){
				int aux = array[i];
				array[i] = array[j];
				array[j] = aux;
			}
		}
		
		int aux = array[j];
		array[j] = pivot;
		array[start] = aux;
		
		if (start < j-1) quicksort(array, start, j-1);
		if (end > j+1) quicksort(array, j+1, end);
		
		return array;
	}
	
	/**
	 * Sort method, first and public part of the Mergesort method for sorting arrays
	 * @param array, the array we want to sort
	 * @return the sorted array
	 */
	
	public static int[] sort(int[] array){
		if (array.length == 1) return array;
		
		int[] vector1 = new int[array.length/2];
		int[] vector2 = new int[array.length - array.length/2];
		for (int i = 0; i < vector1.length; i++){
			vector1[i] = array[i];
		}
		for (int j = 0; j < vector2.length; j++){
			vector2[j] = array[j+vector1.length];
		}
		
		return merge(sort(vector1), sort(vector2));
	}
	
	/**
	 * Merge method, second part of the Mergesort method for sorting arrays
	 * THIS METHOD MUST BE PRIVATE
	 * @param array1 first half of the array
	 * @param array2 second half of the array
	 * @return the merged array
	 */
	private static int[] merge(int[] array1, int[] array2){
		int[] mergedArray = new int[array1.length+array2.length];
		int i = 0;
		while ((array1.length != 0) && (array2.length != 0)){
			if (array1[0] <= array2[0]){
				mergedArray[i++] = array1[0];
				array1 = remove(array1);
				if (array1.length == 0){
					while (array2.length != 0){
						mergedArray[i++] = array2[0];
						array2 = remove(array2);
					}
				}
						
			}
			
			else{
				mergedArray[i++] = array2[0];
				array2 = remove(array2);
				if (array2.length == 0){
					while (array1.length != 0){
						mergedArray[i++] = array1[0];
						array1 = remove(array1);
					}
				}
			}
		}
		
		return mergedArray;
	}
	
	/**
	 * Remove method, third part of the Mergesort method for sorting arrays
	 * THIS METHOD MUST BE PRIVATE
	 * @param array the array whose first element we want to remove
	 * @return the array without the first element
	 */
	private static int[] remove(int[] array){
		int[] vector = new int [array.length-1];
		for (int i = 0; i < vector.length; i++){
			vector[i] = array[i+1];
		}
		
		return vector;
	}
	
	/**
	 * Test class. Choose which to test by removing the //
	 * @param args
	 */
	
	public static void main(String[] args){
		int[] array = {3, 4, 1, 2, 6, 9, 7, 5, 1, 10, 22, 31, 12};
		//quicksort(array, 0, array.length-1);
		//array = sort(array);
		for (int i = 0; i < array.length; i++){
			System.out.print(array[i] + " ");
		}
	}
}
