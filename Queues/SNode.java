/**
 * SNode.java
 * Part of the single linked list, queues and stacks families
 *
 * SNode Java class, which contains an element of type String
 * It also contains a pointer to the next node
 * http://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */

package queues;

public class SNode {

	String elem;
	SNode next;
	public SNode(String elem){
		this.elem = elem;
	}
	
}
