/**
 * IQueue.java
 * Part of the queues family
 *
 * Queue Java interface, which contains all the methods to create and use queues
 * https://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */

package queues;

public interface IQueue {

	public void enqueue (String elem);
	
	public String dequeue();
	
	public boolean isEmpty();
	
	public int getSize();
	
	public String front();
}
