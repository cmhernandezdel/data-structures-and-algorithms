/**
 * SQueue.java
 * Part of the queues family
 *
 * SQueue Java class, which contains methods to create and use the queues
 * All of these methods are declared in the IQueue Java interface
 * http://gitlab.com/cmhernandezdel
 * Author: Carlos M. Hernández Delgado
 * At Universidad Carlos III de Madrid
 */

package queues;

public class SQueue implements IQueue {

	SNode front, last;
	
	public void enqueue(String elem) {
		SNode newNode = new SNode(elem);
		if (isEmpty()){
			front = newNode;
			last = newNode;
			System.out.println("Element added successfully");
			return;
		}
		last.next = newNode;
		last = newNode;
		System.out.println("Element queued successfully");
		
	}

	public String dequeue() {
		if (isEmpty()){
			System.out.println("WARNING: Cannot dequeue, queue is empty");
			return null;
		}
		
		String var = front();
		
		if (front == last){
			front = null; 
			last = null;
		}
		
		else{
			front = front.next;
		}
		
		System.out.println("Element dequeued successfully");
		
		return var;
	}

	public boolean isEmpty() {
		return front == null;
	}

	public int getSize() {
		int size = 0;
		for (SNode point = front; point != null; point = point.next) size++;
		return size;
	}

	public String front() {
		if (isEmpty()){
			System.out.println("WARNING: Queue is empty");
			return null;
		}
		else return front.elem;
	}
	
	public String toString(){
		String r = "";
		for (SNode point = front; point != null; point = point.next){
			r += point.elem + " ";
		}
		return r;
	}

}
